#include <iostream>
#include "pion.h"
#include "case.h"
#include "colonne.h"
#include "ligne.h"
#include "diagonale.h"
#include "grillemorpion.h"
#include "jeu.h"

using namespace std;

int main()
{
    //-- Les cases du morpions (utile aussi pour le puissance 4)
    // -- cXY avec X la ligne , Y la colonne
    Case c00 = Case();
    Case c01 = Case();
    Case c02 = Case();
    Case c10 = Case();
    Case c11 = Case();
    Case c12 = Case();
    Case c20 = Case();
    Case c21 = Case();
    Case c22 = Case();

    Colonne col1 = Colonne(&c00,&c10,&c20);
    Colonne col2 = Colonne(&c01,&c11,&c21);
    Colonne col3 = Colonne(&c02,&c12,&c22);
    Ligne lg1 = Ligne(&c00,&c01,&c02);
    Ligne lg2 = Ligne(&c10,&c11,&c12);
    Ligne lg3 = Ligne(&c20,&c21,&c22);
    Diagonale dg1 = Diagonale(&c00,&c11,&c22);
    Diagonale dg2 = Diagonale(&c02,&c11,&c20);

    GrilleMorpion laGrilleMorpion = GrilleMorpion(&lg1,&lg2,&lg3,&col1,&col2,&col3,&dg1,&dg2);

    //Avec case puissance 4 : ajout des 28 cases totales
    Case p09 = Case();
    Case p10 = Case();
    Case p11 = Case();
    Case p12 = Case();
    Case p13 = Case();
    Case p14 = Case();
    Case p15 = Case();
    Case p16 = Case();
    Case p17 = Case();
    Case p18 = Case();
    Case p19 = Case();
    Case p20 = Case();
    Case p21 = Case();
    Case p22 = Case();
    Case p23 = Case();
    Case p24 = Case();
    Case p25 = Case();
    Case p26 = Case();
    Case p27 = Case();

    std::vector<Ligne*> listDeLignesPuissance4;

    Ligne puiss4ligne1 = Ligne(Case::toutesLesCases,7,0);
    listDeLignesPuissance4.push_back(&puiss4ligne1);
    Ligne puiss4ligne2 = Ligne(Case::toutesLesCases,7,7);
    listDeLignesPuissance4.push_back(&puiss4ligne2);
    Ligne puiss4ligne3 = Ligne(Case::toutesLesCases,7,14);
    listDeLignesPuissance4.push_back(&puiss4ligne3);
    Ligne puiss4ligne4 = Ligne(Case::toutesLesCases,7,21);
    listDeLignesPuissance4.push_back(&puiss4ligne4);

    std::vector<Colonne*> listDeColonnesPuissance4;

    Colonne puiss4Colonne1 = Colonne(Case::toutesLesCases,4,7,0);
    listDeColonnesPuissance4.push_back(&puiss4Colonne1);
    Colonne puiss4Colonne2 = Colonne(Case::toutesLesCases,4,7,1);
    listDeColonnesPuissance4.push_back(&puiss4Colonne2);
    Colonne puiss4Colonne3 = Colonne(Case::toutesLesCases,4,7,2);
    listDeColonnesPuissance4.push_back(&puiss4Colonne3);
    Colonne puiss4Colonne4 = Colonne(Case::toutesLesCases,4,7,3);
    listDeColonnesPuissance4.push_back(&puiss4Colonne4);
    Colonne puiss4Colonne5 = Colonne(Case::toutesLesCases,4,7,4);
    listDeColonnesPuissance4.push_back(&puiss4Colonne5);
    Colonne puiss4Colonne6 = Colonne(Case::toutesLesCases,4,7,5);
    listDeColonnesPuissance4.push_back(&puiss4Colonne6);
    Colonne puiss4Colonne7 = Colonne(Case::toutesLesCases,4,7,6);
    listDeColonnesPuissance4.push_back(&puiss4Colonne7);

    std::vector<Diagonale*> listDeDiagonalesPuissance4;

    Diagonale puiss4Diagonale1 = Diagonale(Case::toutesLesCases,4,7,0,1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale1);
    Diagonale puiss4Diagonale2 = Diagonale(Case::toutesLesCases,4,7,1,1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale2);
    Diagonale puiss4Diagonale3 = Diagonale(Case::toutesLesCases,4,7,2,1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale3);
    Diagonale puiss4Diagonale4 = Diagonale(Case::toutesLesCases,4,7,3,1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale4);
    Diagonale puiss4Diagonale5 = Diagonale(Case::toutesLesCases,4,7,6,-1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale5);
    Diagonale puiss4Diagonale6 = Diagonale(Case::toutesLesCases,4,7,5,-1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale6);
    Diagonale puiss4Diagonale7 = Diagonale(Case::toutesLesCases,4,7,4,-1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale7);
    Diagonale puiss4Diagonale8 = Diagonale(Case::toutesLesCases,4,7,3,-1);
    listDeDiagonalesPuissance4.push_back(&puiss4Diagonale8);

    GrillePuissance4 laGrilleDePuissance4 = GrillePuissance4(listDeLignesPuissance4,listDeColonnesPuissance4,listDeDiagonalesPuissance4);

    Jeu myGame = Jeu(&laGrilleMorpion,&laGrilleDePuissance4);

    bool stayLoop  = true;
    std::string choix;
    while(stayLoop){
        std::cout << "Une partie ? 1 pour Morpion, 2 pour Puissance 4, 0 pour Quitter" << std::endl;
        std::cin >> choix;
        for (int i = 0; i < choix.length(); i++) if (isdigit(choix[i]) == false) choix = "-1";
        switch(stoi(choix)){
            case 1 : myGame.StartGame("Morpion");
                    myGame.CleanGame();
                    break;
            case 2 : myGame.StartGame("Puissance 4");
                    myGame.CleanGame();
                    break;
            case 0 : stayLoop = false;
                    break;
            default : std::cout << "MAUVAISE VALEUR" << std::endl;
        }

    }
}
