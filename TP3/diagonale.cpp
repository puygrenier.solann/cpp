#include "diagonale.h"

Diagonale::Diagonale(Case* caseHautGauche,Case* caseMilieu, Case* caseBasDroite){
    m_caseDiagonale.push_back(caseHautGauche);
    m_caseDiagonale.push_back(caseMilieu);
    m_caseDiagonale.push_back(caseBasDroite);
}
Diagonale::Diagonale(std::vector<Case*> allCase,int caseaMettre,int longueurLigne,int index,int sens){
    int cnt = 0;
    int cntNbCaseMis = 0;
    int prochianIndex = cntNbCaseMis*(longueurLigne+sens) + index;
    for(Case* i : allCase){
        if((cntNbCaseMis<caseaMettre) && (prochianIndex == cnt)){
            m_caseDiagonale.push_back(i);
            cntNbCaseMis++;
            prochianIndex = cntNbCaseMis*(longueurLigne+sens) + index;
        }
        cnt++;
    }
}


Case Diagonale::GetCaseParIndexe(int i){
    return *m_caseDiagonale[i];
}

bool Diagonale::EstElleComplete(){
    int tmp = GetCaseParIndexe(0).GetPion().GetNumeroDuJoueur();
    if(tmp<=0){return false;}
    for (int i=1;i<m_caseDiagonale.size();i++){
        if( tmp !=  this->GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur()){return false;}
    }
    return true;
}
bool Diagonale::EstElleComplete(int idJoueur){
    for (int i=0;i<m_caseDiagonale.size();i++){
        if( idJoueur !=  this->GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur()){return false;}
    }
    return true;
}

