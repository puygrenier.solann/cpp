#ifndef CASE_H
#define CASE_H

#include <iostream>
#include <vector>
#include <string>
#include "pion.h"


class Case
{

public:
    //#Attribut
    Pion m_lePionDansLaCase = Pion(); // Par default le pion vide
    bool m_caseDisponible = true;
    //#static
    static std::vector<Case*> toutesLesCases;
    static void AfficheToutesLesCases(){for(Case* i : toutesLesCases) std::cout << i->ToString();}
    static void InitialiseToutesLesCaseA(int valeur){for(Case* i : Case::toutesLesCases){i->ChangePionJoueur(valeur);}}

    //#Constructeur
    Case();
    //#Methode
    inline std::string ToString(){return "| "+ m_lePionDansLaCase.ToString()+ " |";}
    inline void ChangePionJoueur(int i){m_lePionDansLaCase.SetNumeroDuJoueur(i);}
    inline Pion GetPion(){return m_lePionDansLaCase;}
private :
};

#endif // CASE_H
