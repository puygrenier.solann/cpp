#ifndef LIGNE_H
#define LIGNE_H

#include <vector>
#include <string>
#include "case.h"

class Ligne
{
public:
    //#Constructor
    Ligne(Case* caseGauche,Case* caseMilieu,Case* caseDroite);
    Ligne(std::vector<Case*> allCase,int caseParLigne,int index);
    //#
    Case GetCaseParIndexe(int i);
    //#Methodes
    std::string ToString(){
        std::string tmp = "";
        for(Case* i : m_ligneCase) tmp = tmp + i->ToString();
        return tmp;
    }
    bool EstElleComplete();
    bool EstElleComplete(int idPlayer);
    std::vector<Case*> m_ligneCase;
private:

};

#endif // LIGNE_H
