#include "grillepuissance4.h"

GrillePuissance4::GrillePuissance4(std::vector<Ligne*> vectorOfLignes,std::vector<Colonne*> vectorOfColonnes,std::vector<Diagonale*> vectorOfDiagonales){
    m_colonnes = vectorOfColonnes;
    m_lignes = vectorOfLignes;
    m_diagonales = vectorOfDiagonales;
}
void GrillePuissance4::Affiche(){
    for (auto& element : m_lignes){
        std::cout << element->ToString()<<std::endl;
    }
    std::cout << "---------------------------------------------------" <<std::endl;
}
std::string GrillePuissance4::ToString(){
    std::string tmp = "";
    for(Ligne* i :m_lignes) tmp = tmp + "\n" + i->ToString();
    return tmp;
}
bool GrillePuissance4::LigneComplete(Ligne lg,int idJoueur){
    for(int i =0;i<=(lg.m_ligneCase.size()/2);i++){
        if(lg.GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur() == idJoueur){
            if ( (lg.GetCaseParIndexe(i+1).GetPion().GetNumeroDuJoueur() == idJoueur) &&
                (lg.GetCaseParIndexe(i+2).GetPion().GetNumeroDuJoueur() == idJoueur) &&
                    (lg.GetCaseParIndexe(i+3).GetPion().GetNumeroDuJoueur() == idJoueur)){
                return true;
            }
        }
    }
    return false;
}
bool GrillePuissance4::ColonneComplete(Colonne col,int idJoueur){
    if(col.GetCaseParIndexe(0).GetPion().GetNumeroDuJoueur() == idJoueur){
        if ( (col.GetCaseParIndexe(1).GetPion().GetNumeroDuJoueur() == idJoueur) &&
             (col.GetCaseParIndexe(2).GetPion().GetNumeroDuJoueur() == idJoueur) &&
             (col.GetCaseParIndexe(3).GetPion().GetNumeroDuJoueur() == idJoueur) ){
                return true;
            }
        }
    return false;
}
bool GrillePuissance4::ColonnePleine(Colonne col,int idJoueur){
    return col.EstElleComplete(idJoueur);
}

bool GrillePuissance4::DiagonaleComplete(Diagonale dg,int idJoueur){
    if(dg.GetCaseParIndexe(0).GetPion().GetNumeroDuJoueur() == idJoueur){
        if ( (dg.GetCaseParIndexe(1).GetPion().GetNumeroDuJoueur() == idJoueur) &&
             (dg.GetCaseParIndexe(2).GetPion().GetNumeroDuJoueur() == idJoueur) &&
             (dg.GetCaseParIndexe(3).GetPion().GetNumeroDuJoueur() == idJoueur) ){
                return true;
            }
        }
    return false;
}
void GrillePuissance4::DeposerJeton(Case* aCase,int idJoueur){
    aCase->m_lePionDansLaCase.SetNumeroDuJoueur(idJoueur);
}
bool GrillePuissance4::VictoireJoueur(int idJoueur){
    for(Colonne* i : m_colonnes) if(ColonneComplete(*i,idJoueur)) return true;
    for(Ligne* i : m_lignes) if(LigneComplete(*i,idJoueur)) return true;
    for(Diagonale* i : m_diagonales) if(DiagonaleComplete(*i,idJoueur)) return true;
    return false;
}
