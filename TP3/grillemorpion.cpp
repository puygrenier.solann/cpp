#include "grillemorpion.h"


GrilleMorpion::GrilleMorpion(Ligne* lg1,Ligne* lg2, Ligne* lg3,Colonne* col1,Colonne* col2,Colonne* col3,Diagonale* dg1,Diagonale* dg2){
    m_colonnes.push_back(col1);
    m_colonnes.push_back(col2);
    m_colonnes.push_back(col3);
    m_lignes.push_back(lg1);
    m_lignes.push_back(lg2);
    m_lignes.push_back(lg3);
    m_diagonales.push_back(dg1);
    m_diagonales.push_back(dg2);

}

void GrilleMorpion::Affiche(){
    std::cout << "____________________" <<std::endl;
    for (auto& element : m_lignes){
        std::cout << element->ToString()<<std::endl;
    }
    std::cout << "____________________" <<std::endl;
}

void GrilleMorpion::DeposerJeton(Case* aCase,int idPlayer){
    aCase->m_lePionDansLaCase.SetNumeroDuJoueur(idPlayer);
}

bool GrilleMorpion::LigneComplete(Ligne lg,int idPlayer){
    return lg.EstElleComplete(idPlayer);
}
bool GrilleMorpion::LignePleine(Ligne lg){
    return lg.EstElleComplete();
}

bool GrilleMorpion::ColonneComplete(Colonne col,int idPlayer){
    return col.EstElleComplete(idPlayer);
}
bool GrilleMorpion::ColonnePleine(Colonne col){
    return col.EstElleComplete();
}
bool GrilleMorpion::DiagonaleComplete(Diagonale dg,int idPlayer){
    return dg.EstElleComplete(idPlayer);
}
bool GrilleMorpion::DiagonalePleine(Diagonale dg){
    return dg.EstElleComplete();
}

bool GrilleMorpion::CaseEstElleVide(Case aCase){
    if (aCase.GetPion().GetNumeroDuJoueur() == 0) return true;
    else return false;
}

bool GrilleMorpion::VictoireJoueur(int idJoueur){
    for(Colonne* i : m_colonnes) if(ColonneComplete(*i,idJoueur)) return true;
    for(Ligne* i : m_lignes) if(LigneComplete(*i,idJoueur)) return true;
    for(Diagonale* i : m_diagonales) if(DiagonaleComplete(*i,idJoueur)) return true;
    return false;
}

std::string GrilleMorpion::ToString(){
    std::string tmp = "";
    for(Ligne* i :m_lignes) tmp = tmp + "\n" + i->ToString();
    return tmp;
}
