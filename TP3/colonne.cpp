#include "colonne.h"
#include <iostream>

Colonne::Colonne(Case *caseHaute,Case *caseMilieu, Case *caseBas){
    m_colonneCase.push_back(caseHaute);
    m_colonneCase.push_back(caseMilieu);
    m_colonneCase.push_back(caseBas);
}
// Partie III ---
Colonne::Colonne(std::vector<Case*> allCase,int caseAMettre,int longueurLigne,int index){
    int cnt = 0;
    int cntNbCaseMis = 0;
    int prochianIndex = cntNbCaseMis*(longueurLigne) + index;
    for(Case* i : allCase){
        if((cntNbCaseMis<caseAMettre) && (prochianIndex == cnt)){
            m_colonneCase.push_back(i);
            cntNbCaseMis++;
            prochianIndex = cntNbCaseMis*(longueurLigne) + index;
        }
        cnt++;
    }
}
// ----
Case Colonne::GetCaseParIndexe(int i){
    return *m_colonneCase[i];
}

bool Colonne::EstElleComplete(){
    int tmp = GetCaseParIndexe(0).GetPion().GetNumeroDuJoueur();
    if(tmp<=0){return false;}
    for (int i=1;i<m_colonneCase.size();i++){
        if( tmp !=  this->GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur()){return false;}
    }
    return true;
}
bool Colonne::EstElleComplete(int idJoueur){
    for (int i=0;i<m_colonneCase.size();i++){
        if( idJoueur !=  this->GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur()){return false;}
    }
    return true;
}


