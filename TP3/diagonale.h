#ifndef DIAGONALE_H
#define DIAGONALE_H

#include <string>
#include <vector>
#include "case.h"

class Diagonale
{
public:
    Diagonale(Case* caseHautGauche,Case* caseMilieu, Case* caseBasDroite);
    Diagonale(std::vector<Case*> allCase,int caseaMettre,int longueurLigne,int index,int sens);
    Case GetCaseParIndexe(int i);

    std::string ToString() {
        std::string tmp = "" ;
        for(Case* i : m_caseDiagonale)tmp = tmp + i->ToString();
        return tmp;
    }
    bool EstElleComplete();
    bool EstElleComplete(int idJoueur);
private :
    std::vector<Case*> m_caseDiagonale;
};

#endif // DIAGONALE_H
