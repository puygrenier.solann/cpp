#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include <vector>
#include <string>
#include <iostream>

#include "pion.h"
#include "colonne.h"
#include "ligne.h"
#include "diagonale.h"
#include "case.h"

class GrilleMorpion
{
public:
    //#Attribut
    std::vector<Colonne*> m_colonnes;
    std::vector<Ligne*> m_lignes;
    std::vector<Diagonale*> m_diagonales;
    //#Constructeur
    GrilleMorpion(Ligne* lg1,Ligne* lg2, Ligne* lg3,Colonne* col1,Colonne* col2,Colonne* col3,Diagonale* dg1,Diagonale* dg2);
    //#Methodes
    bool CaseEstElleVide(Case aCase);
    void Affiche();
    bool LigneComplete(Ligne lg,int idJoueur);
    bool LignePleine(Ligne lg);
    bool ColonneComplete(Colonne col,int idJoueur);
    bool ColonnePleine(Colonne col);
    bool DiagonaleComplete(Diagonale dg,int idJoueur);
    bool DiagonalePleine(Diagonale dg);
    void DeposerJeton(Case* aCase,int idJoueur);
    bool VictoireJoueur(int idJoueur);
    std::string ToString();
private :
    static int m_counteurDeCase;
    int const LIGNE_MAX = 3;
    int const COLONNE_MAX = 3;


};

#endif // GRILLEMORPION_H
