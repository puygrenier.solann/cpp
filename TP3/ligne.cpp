#include "ligne.h"

Ligne::Ligne(Case* caseGauche,Case* caseMilieu,Case* caseDroite){
    m_ligneCase.push_back(caseGauche);
    m_ligneCase.push_back(caseMilieu);
    m_ligneCase.push_back(caseDroite);
}
// Partie III ----
Ligne::Ligne(std::vector<Case*> allCase,int caseParLigne,int index){
   int cnt = 0;
    for(Case* i : allCase){
        if((cnt>=index)&&(cnt <(index+caseParLigne))){
            m_ligneCase.push_back(i);
        }
        cnt++;
    }
}
// ----

Case Ligne::GetCaseParIndexe(int i){
    return *m_ligneCase[i];
}

bool Ligne::EstElleComplete(){
    int tmp = GetCaseParIndexe(0).GetPion().GetNumeroDuJoueur();
    if(tmp<=0){return false;}
    for (int i=1;i<m_ligneCase.size();i++){
        if( tmp !=  this->GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur()){return false;}
    }
    return true;
}

bool Ligne::EstElleComplete(int idPlayer){
    for (int i=0;i<m_ligneCase.size();i++){
        if( idPlayer !=  this->GetCaseParIndexe(i).GetPion().GetNumeroDuJoueur()){return false;}
    }
    return true;
}

