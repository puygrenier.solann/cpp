#ifndef POINT_H
#define POINT_H

#include <string>
using namespace std;

struct Point{
  //#Attributs
  float x;
  float y;
  //#Constructeurs
  Point(float abscisse,float ordonne);
  Point();
  //#Methodes
  float EcartSurX(Point& pts) const;
  float EcartSurY(Point& pts) const;
  float Distance(Point& pts) const;
  void Afficher();
  void Afficher(const int PRECISION_AFFICHAGE_POINT);
};

#endif // POINT_H
