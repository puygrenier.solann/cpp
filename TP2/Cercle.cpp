#include <math.h>
#include <iostream>
#include "Cercle.h"

const float PI = 3.1415;

Cercle::Cercle(Point centre,int diametre):m_centre(centre),m_diametre(diametre){
}

float Cercle::Perimetre() const {return PI*this->m_diametre;}
float Cercle::Surface() const {return PI*pow((this->m_diametre),2);}

// Utilise la formule du cercle r² = (x - a)² + (y - b)²
// On renvoi vrai si que r² et (x - a)² + (y - b)² sont presque equivalent (inferieur a une precision le plus proche possible de 0 comme 0.00001)
bool Cercle::EstCeQueCePointEstSURLeCercle(Point& point,float const precision){
  float a = (this->GetCentre().x);
  float b = (this->GetCentre().y);
  float r = ( (float)this->GetDiametre() ) /2;
  float rPuissance2 = pow(r,2);
  float testRayon = pow((point.x-a),2) + pow((point.y-b),2);
  if( (fabs(testRayon - rPuissance2) < precision ) ) return true;
  else return false;
}
// Utilise la formule du cercle r² = (x - a)² + (y - b)²
// On renvoi vrai si (x - a)² + (y - b)² est inferieur ou égale au rayon² <=> point dans le cercle
bool Cercle::EstCeQueCePointEstDANSLeCercle(Point& point,float const precision){
  float a = (this->GetCentre().x);
  float b = (this->GetCentre().y);
  float rPuissance2 = pow(this->GetDiametre()/2,2);
  float testRayon = pow((point.x-a),2) + pow((point.y -b),2);
  if( ( (rPuissance2 - testRayon) <= precision ) ) return true;
  else return false;
}

void Cercle::Afficher(){
  std::cout << "Diametre du cercle : " << this->GetDiametre() << endl;
  std::cout << "Origine du cerlce : ";
  this->GetCentre().Afficher();
  std::cout << "Permietre du cercle : " << this->Perimetre() <<endl;
  std::cout << "Surface du cercle : " << this->Surface() << endl;
  cout << " * * * Test de l'appartenenace de point * * *" <<endl;

  Point pointSurCercle(0,2.5);
  Point pointPresqueSurleCercleInterieur(0,2.499);
  Point pointPresqueSurLeCercleExterieur(0,2.5001);
  Point pointDansleCercle(0,2);
  Point pointSurCercleEnNegatif(0,-2.5);
  Point pointPresqueSurleCercleInterieurEnNegatif(0,-2.499);
  Point pointPresqueSurLeCercleExterieurEnNegatif(0,-2.5001);
  Point pointDansleCercleEnNegatif(0,-2);

  cout << "Est ce que le point (0,2.5) est SUR le cercle ? (precision 0.00000001)\t" << this->EstCeQueCePointEstSURLeCercle(pointSurCercle,0.00000001) <<endl ;
  cout <<  "Est ce que le point (0,2.499) est SUR le cercle ? (prescision 0.01)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurleCercleInterieur,0.01) << endl;
  cout <<  "Est ce que le point (0,2.499) est SUR le cercle ? (prescision 0.00001)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurleCercleInterieur,0.00001) << endl;
  cout <<  "Est ce que le point (0,2.5001) est SUR le cercle ? (prescision 0.01)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurLeCercleExterieur,0.01) << endl;
  cout <<  "Est ce que le point (0,2.5001) est SUR le cercle ? (prescision 0.00001)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurLeCercleExterieur,0.00001) << endl;
  cout <<  "Est ce que le point (0,2) est DANS le cercle ? (precision 0.00000001)\t" << this->EstCeQueCePointEstDANSLeCercle(pointDansleCercle,0.001) <<endl;

  cout << "Est ce que le point (0,-2.5) est SUR le cercle ? (precision 0.00000001)\t" << this->EstCeQueCePointEstSURLeCercle(pointSurCercleEnNegatif,0.00000001) <<endl ;
  cout <<  "Est ce que le point (0,-2.499) est SUR le cercle ? (prescision 0.01)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurleCercleInterieurEnNegatif,0.01) << endl;
  cout <<  "Est ce que le point (0,-2.499) est SUR le cercle ? (prescision 0.00001)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurleCercleInterieurEnNegatif,0.00001) << endl;
  cout <<  "Est ce que le point (0,-2.5001) est SUR le cercle ? (prescision 0.01)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurLeCercleExterieurEnNegatif,0.01) << endl;
  cout <<  "Est ce que le point (0,-2.5001) est SUR le cercle ? (prescision 0.00001)\t" << this->EstCeQueCePointEstSURLeCercle(pointPresqueSurLeCercleExterieurEnNegatif,0.00001) << endl;
  cout <<  "Est ce que le point (0,-2) est DANS le cercle ? (precision 0.00000001)\t" << this->EstCeQueCePointEstDANSLeCercle(pointDansleCercleEnNegatif,0.001) <<endl;
}
