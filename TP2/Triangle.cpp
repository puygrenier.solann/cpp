#include <math.h>
#include <algorithm>
#include <iostream>
#include "Triangle.h"

//#Constructeur
//Cosntructeur affectant lors de la construction les cotés dans les attributs correspondants
Triangle::Triangle(Point pts1,Point pts2,Point pts3):m_ptsA(pts1),m_ptsB(pts2),m_ptsC(pts3){
  Segment segA(pts1,pts2);
  Segment segB(pts2,pts3);
  Segment segC(pts3,pts1);

  m_segmentBase = segA.QuelEstLePlusGrandSegment(segB,segC);// Le cote plus grand parmis segA,segB,segC
  m_segmentPetit = segA.QuelEstLePlusPetitSegment(segB,segC);// Le cote plus petit parmis segA,segB,segC

  //affectaction du dernier segment : cote moyen OU egale au plus grand OU egal au plus petit
  if ( (segA.EstIlDifferentDe(m_segmentBase))&&(segA.EstIlDifferentDe(m_segmentPetit)) ) m_segmentMilieu = segA;
  else if (segB.EstIlDifferentDe(m_segmentBase)&&segB.EstIlDifferentDe(m_segmentPetit)) m_segmentMilieu = segB;
  else m_segmentMilieu = segC;
}
//#Methodes
//Renvoi la hauteur du triangle par rapport à la base
float Triangle::Hauteur(){
  float a = this->GetSegmentBase().longueur;
  float b = this->GetSegmentMilieu().longueur;
  float c = this->GetSegmentPetit().longueur;
  return sqrt(pow(c,2) - pow((pow(a,2)-pow(b,2)+pow(c,2))/(2*a),2));
}
float Triangle::Aire(){
  return this->GetSegmentBase().longueur * this->Hauteur() / 2;
}

bool Triangle::EstIlIsocele(){
  float a = this->GetSegmentBase().longueur;
  float b = this->GetSegmentMilieu().longueur;
  float c = this->GetSegmentPetit().longueur;
  if(  ( (a == b) && (b != c) ) || ( (b == c) && (c != a) ) || ( (c == a) && (a != b) )  ){
    return true;
  }
  else return false;
}
bool Triangle::EstIlEquilaterale(){
  float a = this->GetSegmentBase().longueur;
  float b = this->GetSegmentMilieu().longueur;
  float c = this->GetSegmentPetit().longueur;
  if( (a == b) && (b == c) ){
    return true;
  }
  else return false;
}

bool Triangle::EstIlRectangle(){
  float a = this->GetSegmentBase().longueur;
  float b = this->GetSegmentMilieu().longueur;
  float c = this->GetSegmentPetit().longueur;
  if( pow(a,2) == (pow(b,2)+pow(c,2)) ){ return true;}
  else return false;
}

void Triangle::Longueurs(){
  std::cout << "1) Cote Base " << endl;
  this->GetSegmentBase().Afficher() ;
  std::cout << "2) Cote petit " << endl;
  this->GetSegmentPetit().Afficher();
  std::cout << "3) Cote restant  " << endl;
  this->GetSegmentMilieu().Afficher();
}

void Triangle::Afficher(){
  this->Longueurs();
  std::cout << "Hauteur h: " << this->Hauteur() <<endl;
  std::cout << "Aire : " << this->Aire() <<endl;
  std::cout << "Est il isocele ?\t" << this->EstIlIsocele()<<endl;
  std::cout << "Est il equilaterale ?\t" << this->EstIlEquilaterale()<<endl;
  std::cout << "Est il rectangle  ?\t" << this->EstIlRectangle()<<endl;
}
