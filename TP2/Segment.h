#ifndef SEGMENT_H
#define SEGMENT_H

#include <iostream>
#include "Point.h"
#include <cmath>

const int PRECISION_SEGMENT = 1000;

struct Segment{
  //#Attributs
  Point pts1;
  Point pts2;
  float longueur;
  //#Constructeurs
  Segment(){}
  //Longueur par arrondi (trivial)
  //Par exemple 0.78958 x 100=>78.958=>round()=>78=>78/100=>0.78
  Segment(Point inputPointA,Point inputPointB):pts1(inputPointA),pts2(inputPointB){
    longueur = (round(inputPointA.Distance(inputPointB))*PRECISION_SEGMENT)/PRECISION_SEGMENT;
  }
  //#Methodes
  void Afficher(){
    std::cout<< "=== ===" <<endl;
    pts1.Afficher();
    pts2.Afficher();
    std::cout << "Longueur : " << this->longueur << endl;
    std::cout<< "=== ===" <<endl;
  }
  //Methode renvoyant le plus grand des deux segments (ou lui même si égalité)
  Segment QuelEstLePlusGrandSegment(Segment segment){
    if (this->longueur >= segment.longueur) return *this;
    else return segment;
  }
  //Methode renvoyant le plus grand des trois segments (ou lui même si égalité)
  Segment QuelEstLePlusGrandSegment(Segment segment1,Segment segment2){
    return this->QuelEstLePlusGrandSegment(segment1.QuelEstLePlusGrandSegment(segment2));
  }
  //Methode renvoyant le plus petit des deux segments (ou lui même si égalité)
  Segment QuelEstLePlusPetitSegment(Segment segment){
    if (this->longueur <= segment.longueur) return *this;
    else return segment;
  }
  //Methode renvoyant le plus petit des trois segments (ou lui même si égalité)
  Segment QuelEstLePlusPetitSegment(Segment segment1,Segment segment2){
    return this->QuelEstLePlusPetitSegment(segment1.QuelEstLePlusPetitSegment(segment2));
  }
  //Interoge uniquement sur ses points pts1 et pts2, la longueur est issue de ces points
  bool EstIlDifferentDe(Segment segment){
    if ( (this->pts1.x != segment.pts1.x) || (this->pts1.y != segment.pts1.y) ||
          (this->pts2.x != segment.pts2.x) || (this->pts2.y != segment.pts2.y) ) return true;
    else return false;
  }
};

#endif //SEGMENT_H
