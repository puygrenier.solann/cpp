#include "Point.h"
#include <math.h>
#include <iostream>

//Constructuer
Point::Point(float abscisse,float ordonne):x(abscisse),y(ordonne){}
Point::Point():x(0),y(0){}
//Methodes
float Point::EcartSurX(Point& pts) const{
  float max = this->x > pts.x ? this->x : pts.x;
  float min = this->x < pts.x ? this->x : pts.x;
  return max - min;
}
float Point::EcartSurY(Point& pts) const{
    float max = this->y > pts.y ? this->y : pts.y;
    float min = this->y < pts.y ? this->y : pts.y;
    return max - min;
}
// Utilisation de phytagore : hypo² = coté opp ² + coté adj ² <=> hypo = (CotéOpp² + CotéAdj²)^1/2
float Point::Distance(Point& pts) const{
  float cote1 = pow(EcartSurX(pts),2);
  float cote2 = pow(EcartSurY(pts),2);
  return sqrt(cote1+cote2);
}
void Point::Afficher(){
  cout << "(" + std::to_string(x) +";"+ std::to_string(y)+")" << endl;
}
//Affichage par arrondi trivial
//Par exemple 1.52389 x 100=>152,389=>round()=>152=>152/100=>1.52
void Point::Afficher(const int PRECISION_AFFICHAGE_POINT){
  float xPourAffichage = round(this->x*PRECISION_AFFICHAGE_POINT)/PRECISION_AFFICHAGE_POINT;
  float yPourAffichage = round(this->y*PRECISION_AFFICHAGE_POINT)/PRECISION_AFFICHAGE_POINT;
  std::cout <<  "(" << xPourAffichage << ";" << yPourAffichage << ")" << endl;
}
