#include "Rectangle.h"
#include <iostream>

Rectangle::Rectangle(int longueur,int largeur): m_largeur(largeur),m_longueur(longueur){
}
Rectangle::Rectangle(int longueur,int largeur,Point point):m_largeur(largeur),m_longueur(longueur),m_coinSuperieurGauche(point){
}

int Rectangle::Perimetre(){
  return GetLargeur()*2+GetLongueur()*2;
}

int Rectangle::Surface(){
  return GetLargeur()*GetLongueur();
}

void Rectangle::Afficher(){
    cout << "=== ==="<<endl;
    cout << "Longueur :"<< this -> GetLongueur() <<endl;
    cout << "Largeur :" << this -> GetLargeur() <<endl;
    cout << "Perimetre :" << this->Perimetre() <<endl;
    cout << "Surface :" << this ->Surface() <<endl;
    Point pts = this->GetCoinSuperieurGauche();
    cout << "Coin Superieur Gauche :" ;
    pts.Afficher();

    cout << "   X ----- " << this->GetLongueur() << "----- X"<< endl;
    cout << "   |"<< endl;
    cout << "   " << this->GetLargeur()<< endl;
    cout << "   |"<< endl;
    cout << "   X ----- "<< this->GetLongueur() << "----- X"<< endl;
}
