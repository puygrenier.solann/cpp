TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Cercle.cpp \
        Point.cpp \
        Rectangle.cpp \
        Triangle.cpp \
        main.cpp

HEADERS += \
    Cercle.h \
    Point.h \
    Rectangle.h \
    Segment.h \
    Triangle.h
