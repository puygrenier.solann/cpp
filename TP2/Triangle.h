#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Point.h"
#include "Segment.h"

class Triangle{
public :
  //#Constructeur : affectation de la base, coté le plus petit et le dernier coté lors de la construction
  Triangle(Point pts1,Point pts2,Point pts3);

  //#Getters et Setters
  inline Point GetPointA(){return m_ptsA;}
  inline void SetPointA(Point pts1){m_ptsA = pts1;}
  inline Point GetPointB(){return m_ptsB;}
  inline void SetPointB(Point pts2){m_ptsB = pts2;}
  inline Point GetPointC(){return m_ptsC;}
  inline void SetPointC(Point pts3){m_ptsC = pts3;}

  inline Segment GetSegmentBase(){return m_segmentBase;}          // Retourne le segment ayant la longueur la plus grande
  inline Segment GetSegmentPetit(){return m_segmentPetit;}        // Retourne le segment ayant la longueur la plus petite
  inline Segment GetSegmentMilieu(){return m_segmentMilieu;}      // Retourne le segment ayant la longueur restante

  float Hauteur();
  float Aire();

  bool EstIlIsocele();
  bool EstIlEquilaterale();
  bool EstIlRectangle();

  void Longueurs();  // Valeur disponible plutot via Segment : this->.GetSegment____().longueur
  void Afficher();
private :
  Segment m_segmentBase; //Cote le plus grand
  Segment m_segmentMilieu; //Cote moyen OU egale au plus grand OU egal au plus petit
  Segment m_segmentPetit; //Cote le plus petit

  Point m_ptsA;
  Point m_ptsB;
  Point m_ptsC;

};
#endif
