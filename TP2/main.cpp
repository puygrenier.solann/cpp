#include <iostream>

#include "Point.h"
#include "Rectangle.h"
#include "Segment.h"
#include "Triangle.h"
#include "Cercle.h"

using namespace std;

int main()
{

    std::cout << "______ Point ______" << endl;
    Point myPointA(0,0);
    Point myPointB(10.5,16.125891);
    myPointA.Afficher();
    myPointB.Afficher();
    myPointB.Afficher(10000);
    std::cout << "Distance entre A et B :" << myPointA.Distance(myPointB) << endl;
    std::cout << "Ecart en abscisse entre A et B :" << myPointA.EcartSurX(myPointB) << endl;
    std::cout << "Ecart en ordonnee entre A et B :" << myPointA.EcartSurY(myPointB) << endl;

    cout << "______ Rectangle ______" << endl;
    std:: cout << " * * * Rectangle 1 * * * " <<endl;
    Rectangle rectangle1(12,13); //implicit declaration
    rectangle1.Afficher();
    std:: cout << " * * * Rectangle 2 * * * " <<endl;
    Rectangle rectangle2 = Rectangle(10,5,myPointB);//explicite declaration
    rectangle2.Afficher();
    std:: cout << " * * * Bonus * * * " <<endl;
    cout << "rectangle1.EstCeQueLaSurfaceLaPlusGrandeEstCelleDe(rectangle2)=" << rectangle1.EstCeQueLaSurfaceLaPlusGrandeEstCelleDe(rectangle2)<<endl;
    cout << "rectangle2.EstCeQueLaSurfaceLaPlusGrandeEstCelleDe(rectangle1)=" << rectangle2.EstCeQueLaSurfaceLaPlusGrandeEstCelleDe(rectangle1)<<endl;
    cout << "rectangle1.EstCeQueLePerimetreLePlusGrandEstCeluiDe(rectangle2)=" << rectangle1.EstCeQueLePerimetreLePlusGrandEstCeluiDe(rectangle2) << endl;
    cout << "rectangle2.EstCeQueLePerimetreLePlusGrandEstCeluiDe(rectangle1)=" << rectangle2.EstCeQueLePerimetreLePlusGrandEstCeluiDe(rectangle1) << endl;

    cout << "______ Triangle ______" << endl;
    std:: cout << " * * * Triangle rectangle * * * " << endl;
    Point a(0,0);
    Point b(3,0);
    Point c(3,4);
    Triangle triRect(a,b,c);
    triRect.Afficher();
    std:: cout << " * * * Triangle equilaterale * * * " << endl;
    Point A1(0,0);
    Point B1(10,0);
    Point C1(5,8.66);
    Triangle triEqui(A1,B1,C1);
    triEqui.Afficher();
    std:: cout << " * * * Triangle scalene * * * " << endl;
    Point A2(0,0);
    Point B2(6,0);
    Point C2(2.175,2.066);
    Triangle tri(A2,B2,C2);
    tri.Afficher();
    std:: cout << " * * * Triangle isocele v1 (long/court/court)* * * " << endl;
    Point A3(0,0);
    Point B3(9,0);
    Point C3(4.5,2.17945);
    Triangle triIsocel(A3,B3,C3);
    triIsocel.Afficher();

    std:: cout << " * * * Triangle isocele v2 (court/long/long)* * * " << endl;
    Point A4(-1,0);
    Point B4(1,0);
    Point C4(0,12);
    Triangle triIsocelV2(A4,B4,C4);
    triIsocelV2.Afficher();

    cout << "______ Cercle ______" << endl;
    Point pointO(0,0);
    Cercle monCercle(pointO,5);
    monCercle.Afficher();
}
