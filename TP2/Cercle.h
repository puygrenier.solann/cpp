#ifndef CERCLE_H
#define CERCLE_H

#include "Point.h"

class Cercle{
public :
  //#Constructuer
  Cercle(Point centre,int diametre);
  //#Getters et Setters
  inline Point GetCentre(){return m_centre;}
  inline void SetCentre(Point ptsCentre){m_centre = ptsCentre;}
  inline int GetDiametre(){return m_diametre;}
  inline void SetDiametre(int diametre){m_diametre = diametre;}
  //#Methodes
  float Perimetre() const;
  float Surface() const;
  bool EstCeQueCePointEstSURLeCercle(Point& point,float const precision);
  bool EstCeQueCePointEstDANSLeCercle(Point& point,float const precision);

  void Afficher();
private :
  Point m_centre;
  int m_diametre;
};

#endif
