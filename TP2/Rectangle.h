#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Point.h"

class Rectangle{

public:
    //Constructeurs
    Rectangle(int longueur,int largeur);
    Rectangle(int longueur,int largeur,Point point);
    //Getters et Setters
    inline int GetLongueur(){return m_longueur;}
    inline void SetLongueur(int longueur){m_longueur = longueur;}
    inline int GetLargeur(){return m_largeur;}
    inline void SetLargueur(int largeur){m_largeur = largeur;}
    inline Point GetCoinSuperieurGauche(){return m_coinSuperieurGauche;}
    inline void SetCoinSuperieurGauche(Point lePoint){m_coinSuperieurGauche = lePoint;}
    //Methodes
    int Perimetre();
    int Surface();
    void Afficher();

    //Methodes Bonus
    bool EstCeQueLePerimetreLePlusGrandEstCeluiDe(Rectangle unRectangle){
      return unRectangle.Perimetre() > this-> Perimetre() ? true : false ;
    }
    bool EstCeQueLaSurfaceLaPlusGrandeEstCelleDe(Rectangle unRectangle){
      return unRectangle.Surface() > this->Surface() ? true : false ;
    }
private:
    int m_largeur;
    int m_longueur;

    Point m_coinSuperieurGauche;
};


#endif // RECTANGLE_H
